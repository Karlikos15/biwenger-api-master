﻿

using bewinger_api_master.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace bewinger_api_master.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureMySqlContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(config.GetConnectionString("DefaultConnection"),
                a => a.MigrationsAssembly("biwenger-api-master.Data")));
        }

        public static void ConfigureMvc(this IServiceCollection services)
        {
            services.AddRazorPages();

            //services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        //public static void ConfigureIdentityCore(this IServiceCollection services)
        //{
        //    var identityBuilder = services.AddIdentityCore<IdentityUser>(o =>
        //    {
        //        // configure identity options
        //        o.Password.RequireDigit = false;
        //        o.Password.RequireLowercase = false;
        //        o.Password.RequireUppercase = false;
        //        o.Password.RequireNonAlphanumeric = false;
        //        o.Password.RequiredLength = 6;
        //    });

        //    identityBuilder = new IdentityBuilder(identityBuilder.UserType, typeof(IdentityRole), identityBuilder.Services);
        //    identityBuilder.AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

        //}

        public static void ConfigureServicesInstances(this IServiceCollection services, IConfiguration config)
        {
            //Identity managers
            services.TryAddScoped<SignInManager<IdentityUser>, SignInManager<IdentityUser>>();
            services.TryAddScoped<UserManager<IdentityUser>, UserManager<IdentityUser>>();
            services.TryAddScoped<RoleManager<IdentityRole>, RoleManager<IdentityRole>>();

        }

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo()
                {
                    Title = "biwenger_api_master",
                    Version = "v1"
                });
            });
        }

        public static void ConfigurePolicies(this IServiceCollection services)
        {
            // Examples
            services.AddAuthorization(options => options.AddPolicy("Trusted", policy => policy.RequireClaim("Employee", "Mosalla")));
            services.AddAuthorization(options => options.AddPolicy("Active", policy => policy.RequireClaim("IsActive")));
        }

    }
}
