﻿using bewinger_api_master.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace bewinger_api_master.Data
{
    public class ApplicationDbContext : DbContext
    {
        private readonly IConfiguration _config;

        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        //public override DbSet<IdentityUser> Users { get; set; }
        //public DbSet<Player> Players { get; set; }
        //public DbSet<Away> Aways { get; set; }
        //public DbSet<Competition> Competitions { get; set; }
        public DbSet<Event> Events { get; set; }
        //public DbSet<Home> Homes { get; set; }
        //public DbSet<Match> Matchs { get; set; }
        //public DbSet<New> News { get; set; }
        //public DbSet<Report> Reports { get; set; }
        //public DbSet<Round> Rounds { get; set; }
        //public DbSet<Season> Seasons { get; set; }
        //public DbSet<Team> Teams { get; set; }
        //public DbSet<Core.Entities.Thread> Threads { get; set; }
        //public DbSet<Offer> Offers { get; set; }


        //public DbSet<LineUp> LineUps { get; set; }
        //public DbSet<Market> Markets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        public override DbSet<TEntity> Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
